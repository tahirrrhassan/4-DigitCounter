#include <EEPROM.h>
#include <EmSevenSegment.h>

#define page 8
#define up 7
#define en 6
#define type 5 
#define reset 4 
#define dir 3
#define pls 2 
#define out1 9
#define out2 10
#define msecond 14
#define second 15
#define minute 16

EmSevenSegment disp(4,'A',18,17,19); //DATA-CLOCK-STROBE
String final, str1;
int count=0,preset=0,val=0;
int a=0,c=0,msec=0,sec=0,mint=0;
boolean g=0,h=0,b=0,d=0,i=0,j=0,k=0,ms=0,m=0,s=0;
int w,x,y,z;

void setup() {
  pinMode(page, INPUT_PULLUP);
  pinMode(up, INPUT_PULLUP);
  pinMode(en, INPUT_PULLUP);
  pinMode(type, INPUT_PULLUP);
  pinMode(pls, INPUT_PULLUP);
  pinMode(reset, INPUT_PULLUP);
  pinMode(dir, INPUT_PULLUP);
  pinMode(msecond, INPUT_PULLUP);
  pinMode(second, INPUT_PULLUP);
  pinMode(minute, INPUT_PULLUP);
  
  pinMode(out1, OUTPUT);
  pinMode(out2, OUTPUT);
  preset = (EEPROM.read(24))*100;
  preset += EEPROM.read(25);
  count = (EEPROM.read(26))*100;
  count += EEPROM.read(27);
  digitalWrite(out1, LOW);
  digitalWrite(out2, LOW);
  disp.print("0000");
  delay(1000);
  attachInterrupt(digitalPinToInterrupt(pls), pulse, FALLING); 
}

void pulse(){
  if(a==0){
    if (b) {
      if (count > 0) count--;
    }
    else{
      if(i){
        if (count < 9999) count++;
      }
      else{
        if (count < preset) count++;
      }
    }
  }
}

void Display(){
  //str1 (COUNT)
  if (str1.length()==1) str1 = "000"+str1;
  else if (str1.length()==2) str1 = "00"+str1;
  else if (str1.length()==3) str1 = "0"+str1;
  
  final = str1.substring(3);
  final += str1.substring(2,3);
  final += str1.substring(1,2);
  final += str1.substring(0,1);
  
  disp.print(final); // Print the number
}

void loop() {
  if(count != val){
    EEPROM.update(26, count/100);
    EEPROM.update(27, count%100);
  }
  val = count;
  if (!digitalRead(en)){
    j=0;
    if((!digitalRead(msecond)) && (digitalRead(second)) && (digitalRead(minute))) ms=1;
    else if((digitalRead(msecond)) && (!digitalRead(second)) && (digitalRead(minute))) s=1;
    else if((digitalRead(msecond)) && (digitalRead(second)) && (!digitalRead(minute))) m=1;
    else{
      ms=0;
      s=0;
      m=0;
    }
    if(k==0) {
      if(!ms && !s && !m) attachInterrupt(digitalPinToInterrupt(pls), pulse, FALLING);
      k=1;
    }
  } else {
    k=0;
    msec=0;
    sec=0;
    mint=0;
    ms=0;
    s=0;
    m=0;
    if(j==0) {
      detachInterrupt(digitalPinToInterrupt(pls));
      j=1;
    }
  }
  count = val;

  if(k && !a){
    if(msec >= 3){
      msec=0;
      sec++;
      if(ms){
        if (b) {
          if (count > 0) count--;
        }
        else{
          if (count < preset) count++;
        }
      }
    }
    else msec++;

    if(sec >= 350){
      sec=0;
      mint++;
      if(s){
        if (b) {
          if (count > 0) count--;
        }
        else{
          if (count < preset) count++;
        }
      }
    }
    else sec++;
    
    if(mint >= 60){
      mint=0;
      if(m){
        if (b) {
          if (count > 0) count--;
        }
        else{
          if (count < preset) count++;
        }
      }
    }
  }
  
  if (!digitalRead(reset)){
    if(i){
      count=0;
      EEPROM.update(26, count/100);
      EEPROM.update(27, count%100);
      delay(500);
    }
    else {
      if (a>0){
        c=0;
        preset = 0;
        count = 0;
        EEPROM.update(26, count/100);
        EEPROM.update(27, count%100);
        EEPROM.update(24, preset/100);
        EEPROM.update(25, preset%100);
        while (!digitalRead(reset));
      }
      else {
        if (b){
          count = preset;
        }
        else count = 0;
        digitalWrite(out1, LOW);
        digitalWrite(out2, LOW);
        //EEPROM.update(24, preset/100);
        //EEPROM.update(25, preset%100);
        //EEPROM.update(26, count/100);
        //EEPROM.update(27, count%100);
        delay(500);
        //while (!digitalRead(reset));
      }
    }
  }
  d = b;
  
  
  if (digitalRead(dir)){ //count up/down
    b=1; //down
  } else{
    b=0; //up
  }
  
  if(b){
    if(d!=b){
      
    }
  }
  
  if (!digitalRead(type)){ //Simple counter
    i=1;
    str1 = String(count);

  }
  //////////////////////////////////////////////////////////////////////
  else { //Logical Counter
  i=0;
  if(!digitalRead(page)){
    c=0;
    if(a==4){
      a=0;
      k=0;
      j=0;
    }
    else a++;
    //if(a==0) attachInterrupt(digitalPinToInterrupt(pls), pulse, FALLING);
    //else detachInterrupt(digitalPinToInterrupt(pls));
    while(!digitalRead(page));
  }
  
  
  if(a>0){
    digitalWrite(out1, LOW);
    digitalWrite(out2, LOW);
    c++;
    
    g=!g;
    
    w = preset/1000;    
    x = (preset/100)%10;
    y = (preset/10)%10;
    z = preset%10;
    
    if(!digitalRead(up)){ //1x100 2x10 3x1 = 123
      c=0;
      h=1;
      switch(a){
        case 1:
          if (z>=9) z=0;
          else z++;
          break;
        case 2:
          if (y>=9) y=0;
          else y++;
          break;
        case 3:
          if (x>=9) x=0;
          else x++;
          break;
        case 4:
          if (w>=9) w=0;
          else w++;
          break;
      }
      preset = w*1000;
      preset += x*100;
      preset += y*10;
      preset += z;
      g=1;
      //delay(250);
    }
    switch(a){
        case 1:
          if(g==1) str1 = String(preset);
          else str1 = String(w) + String(x) + String(y) + " ";
          break;
        case 2:
          if(g==1) str1 = String(preset);
          else str1 = String(w) + String(x) + " " + String(z);
          break;
        case 3:
          if(g==1) str1 = String(preset);
          else str1 = String(w) + " " + String(y) + String(z);
          break;
        case 4:
          if(g==1) str1 = String(preset);
          else str1 = " " + String(x) + String(y) + String(z);
          break;
        
      }
      if(c>=15){
        c=0;
        a=0;
        k=0;
        j=0;
      }
      delay(250);
  }
  else { //Main Display
    if(h==1){
      EEPROM.update(24, preset/100);
      EEPROM.update(25, preset%100);
      EEPROM.update(26, count/100);
      EEPROM.update(27, count%100);
      h=0;
      delay(100);
    }
    str1 = String(count);

    if(!b) {
      if (count > 0) digitalWrite(out1, HIGH);
      else digitalWrite(out1, LOW);
      
      if (count >= preset){
        digitalWrite(out2, HIGH);
        g=!g;
        if(g==1) str1 = String(count);
        else str1 = "    ";
        delay(250);
      }
      else digitalWrite(out2, LOW);
    }
    else {
      if (count < preset) digitalWrite(out1, HIGH);
      else digitalWrite(out1, LOW);
      
      if (count <= 0){
        digitalWrite(out2, HIGH);
        g=!g;
        if(g==1) str1 = String(count);
        else str1 = "    ";
        delay(250);
      }
      else digitalWrite(out2, LOW);
    }
  }
  ////////////////////////////////////
  }
  Display();
  
}
